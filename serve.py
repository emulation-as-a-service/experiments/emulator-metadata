#!/usr/bin/env python3

import http.server


class Handler(http.server.SimpleHTTPRequestHandler):
    def send_response(self, code, message=None):
        super().send_response(code, message)
        self.send_header("cache-control", "no-cache")


bind = "localhost"
port = 8080

print(f"http://{bind}:{port}")

http.server.test(Handler, bind=bind, port=port)
