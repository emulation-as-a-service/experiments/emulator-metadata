import { LitElement, html, css } from "https://esm.sh/lit";
const emus = await (await fetch("emulators.json")).json();

function upgradeToHttps(url) {
  const url2 = new URL(url, location);
  if (url2.hostname !== "localhost") url2.protocol = "https";
  return String(url2);
}

function toRegistry(url) {
  const url2 = new URL(url);
  if (url2.host !== "gitlab.com") return;
  url2.host = "registry.gitlab.com";
  const tag = url2.pathname.match(/\/-\/(?:.*?)\/(.*?)(?:$|\/)/)?.[1];
  url2.pathname = url2.pathname.replace(/\/$/, "").replace(/\/-\/.*/, "");
  return `${url2.host}${url2.pathname}${tag ? `:${tag}` : ""}`;
}

customElements.define(
  "emus-1",
  class extends LitElement {
    render() {
      return html`
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF"
          crossorigin="anonymous"
        />
        <header class="text-center container">
          <h1 class="fw-bold">List Of Emulators</h1>
        </header>
        <center>
          <a href="https://gitlab.com/emulation-as-a-service/emulators"
            >EaaS emulator repository overview</a
          >
          |
          <a
            href="https://gitlab.com/groups/emulation-as-a-service/emulators/-/container_registries"
            >EaaS emulator image overview</a
          >
          |
          <a
            href="https://gitlab.com/emulation-as-a-service/experiments/emulator-metadata"
            >Source of this page</a
          >
        </center>

        <div class="album py-5 bg-light">
          <div class="container">
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
              ${emus.map(
                (emu) => html`
                  <div class="col">
                    <div class="card shadow-sm">
                      <div class="card-body">
                        <h2 class="card-title">
                          <a href="${emu.Logo}">
                            <img
                              ?hidden=${!emu.Logo?.length}
                              src="${upgradeToHttps(emu.Logo)}"
                              style="float: right; max-width: 30%;"
                            />
                          </a>
                          <a href="${emu.Title_URL}"> ${emu.Item[0].title} </a>
                        </h2>
                        <p>${emu.Item[0].description}</p>
                        <dl>
                          <div ?hidden=${!emu.website?.length}>
                            <dt>official website:</dt>
                            <dd>
                              <ul>
                                ${emu.website?.map(
                                  (website) =>
                                    html`
                                      <li>
                                        <a href="${website.url}"
                                          >${website.title}</a
                                        >
                                      </li>
                                    `
                                )}
                              </ul>
                            </dd>
                          </div>
                          <div ?hidden=${!emu.Platform?.length}>
                            <dt>emulates:</dt>
                            <dd>
                              <ul>
                                ${emu.Platform?.map(
                                  (platform) =>
                                    html`
                                      <li>
                                        <a href="${platform.url}"
                                          >${platform.title}</a
                                        >
                                      </li>
                                    `
                                )}
                              </ul>
                            </dd>
                          </div>
                          <div ?hidden=${!emu.Copyright?.length}>
                            <dt>copyright license:</dt>
                            <dd>
                              <ul>
                                ${emu.Copyright?.map(
                                  (copyright) =>
                                    html`
                                      <li>
                                        <a href="${copyright.url}"
                                          >${copyright.title}</a
                                        >
                                      </li>
                                    `
                                )}
                              </ul>
                            </dd>
                          </div>
                          <div ?hidden=${!emu.SourceCode?.length}>
                            <dt>source code repository:</dt>
                            <dd>
                              <ul>
                                ${emu.SourceCode?.map(
                                  (source) =>
                                    html`
                                      <li>
                                        <a href="${source.url}"
                                          >${source.title}</a
                                        >
                                      </li>
                                    `
                                )}
                              </ul>
                            </dd>
                          </div>
                          <div ?hidden=${!emu.EaasRepository}>
                            <dt>EaaS emulator image (for "Import Emulator"):</dt>
                            <dd>
                            <ul>
                            <li>${toRegistry(emu.EaasRepository)}</li>
                              <li><a href="${
                                emu.EaasRepository
                              }">EaaS emulator repository</a></li>
                              <li><a href="${
                                emu.EaasRepository
                              }/container_registry">EaaS emulator image variants</a></li>
                            </dd>
                          </div>
                      </div>
                    </div>
                  </div>
                `
              )}
            </div>
          </div>
        </div>
      `;
    }
  }
);
