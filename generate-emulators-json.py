#!/usr/bin/env python3

from SPARQLWrapper import SPARQLWrapper, JSON, XML
import json
import os

##SPARQL Abragen mit Variable WikiDataItem und Dictionary emu_dict

def SPARQL_Query0_item(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?item ?itemLabel ?itemDescription
        WHERE {
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        VALUES ?item { $WikiDataItem }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)        #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()  #JSON Einträge werden als Dict. formatiert

    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    for result in results['results']['bindings']:
        dict2 = dict()
        if 'item' in result:
            dict2["title"] = result['itemLabel']['value']
            dict2["description"] = result['itemDescription']['value']
            property_list.append(dict2)
    emu_dict["Item"] = property_list
    return emu_dict

def SPARQL_Query1_platform(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?platform ?platformLabel
        WHERE {
        OPTIONAL{ $WikiDataItem wdt:P4043 ?platform .}
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)        #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()  #JSON Einträge werden als Dict. formatiert

    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    for result in results['results']['bindings']:
        dict2 = dict()
        if 'platform' in result:
            dict2["title"] = result['platformLabel']['value']
            dict2["url"] = result['platform']['value']
            property_list.append(dict2)
    emu_dict["Platform"] = property_list
    return emu_dict

def SPARQL_Query2_website(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?website ?websiteLabel
        WHERE {
        OPTIONAL{ $WikiDataItem wdt:P856 ?website .}
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)        #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()  #JSON Einträge werden als Dict. formatiert

    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    for result in results['results']['bindings']:
        dict2 = dict()
        if 'website' in result:
            dict2["title"] = result['websiteLabel']['value']
            dict2["url"] = result['website']['value']
            property_list.append(dict2)
    emu_dict["website"] = property_list
    return emu_dict

def SPARQL_Query3_copyright(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?copyright ?copyrightLabel
        WHERE {
        OPTIONAL{ $WikiDataItem wdt:P275 ?copyright .}
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)        #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()  #JSON Einträge werden als Dict. formatiert

    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    dict2 = dict()
    for result in results['results']['bindings']:
       if 'copyright' in result:
           dict2["title"] = result['copyrightLabel']['value']
           dict2["url"] = result['copyright']['value']
           property_list.append(dict2)
    emu_dict["Copyright"] = property_list
    return emu_dict

def SPARQL_Query4_logo(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?logo ?logoLabel
        WHERE {
        OPTIONAL{ $WikiDataItem wdt:P154 ?logo .}
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)        #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()  #JSON Einträge werden als Dict. formatiert

    #print(results)
    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    for result in results['results']['bindings']:
        if 'logo' in result:
            property_list.append(result['logoLabel']['value'])
    emu_dict["Logo"] = property_list
    return emu_dict

def SPARQL_Query5_sourcecode(emu_dict, WikiDataItem):
    sparql = SPARQLWrapper("https://query.wikidata.org/bigdata/namespace/wdq/sparql")

    sparql.setQuery("""
        SELECT ?sourcecode ?sourcecodeLabel
        WHERE {
        OPTIONAL{ $WikiDataItem wdt:P856 ?sourcecode .}
        SERVICE wikibase:label { bd:serviceParam wikibase:language "en" }
        }""".replace("$WikiDataItem", f"<{WikiDataItem}>"))
    sparql.setReturnFormat(JSON)                                    #Abfrage wird in JSON Format zurückgegeben
    results = sparql.query().convert()                              #JSON Einträge werden als Dict. formatiert

    #Resultate der Anfrage werden in Emu-dict eingtragen
    property_list = list()
    dict2 = dict()
    for result in results['results']['bindings']:
        if 'sourcecode' in result:
            dict2["title"] = result['sourcecodeLabel']['value']
            dict2["url"] = result['sourcecode']['value']
            property_list.append(dict2)
    emu_dict["SourceCode"] = property_list
    return emu_dict

# Liste mit den Dateien erstellen und dann nacheinander auslesen
def dateien_auslesen(path):
    file_list=sorted(os.listdir(path)) #Liste mit JSON Dateien
    emu_list = list()
    
    for file in file_list:
        json_datei = open(path +'/' +file, 'r')                     #JSON Dateien werden geöffnet und gelesen

        python_dict = json.load(json_datei)                         #JSON Datei in Python-Dict Format umwandeln
        WikiDataItem = python_dict['WikiDataItem']                  #WikiDataItem und EmulatorType herauslesen
        EmulatorType = python_dict['eaasEmulatorType']
        emu_dict = dict()                                           #Dictionary in das für jeden Emulator die Resultate abgelegt werden
        
    
        emu_dict["Title"] = EmulatorType
        emu_dict["Title_URL"] = WikiDataItem
        emu_dict["Title_URL"] = WikiDataItem
        emu_dict["EaasRepository"] = python_dict['eaasRepository']
        SPARQL_Query0_item(emu_dict, WikiDataItem)
        SPARQL_Query1_platform(emu_dict, WikiDataItem)
        SPARQL_Query2_website(emu_dict, WikiDataItem)
        SPARQL_Query3_copyright(emu_dict, WikiDataItem)
        SPARQL_Query4_logo(emu_dict, WikiDataItem)
        SPARQL_Query5_sourcecode(emu_dict, WikiDataItem)            #Aufruf der SPARQL Abfrage mit WikiDataItem als Variable
        emu_list.append(emu_dict)

    file = open("./emulators.json", "a")                       #Ausgabe wird in JSON-Datei abgelegt bzw. angehängt
    json.dump(emu_list,file, indent=4)
    file.close()

#Ausführen
path: str = r"data"                                         #Pfad bei dem die Dateien liegen
open("./emulators.json", "w").close()                          #Dateiinhalt wird zum 'Überschreiben' gelöscht
dateien_auslesen(path)
